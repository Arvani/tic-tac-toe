require 'spec_helper'

RSpec.describe Players::Computer do

  describe "inheritance" do
    it { expect(described_class).to be < Player }
  end

  describe "methods" do
    let(:board) { build :board }
    let(:board_view) { build :board_view, board: board }
    # Testando métodos com dificuldade hard, onde sabemos
    # que nenhum dado será aleatório
    subject(:player) { build :player_computer_hard, board_view: board_view }

    describe '#play' do
      context 'when has middle position free' do
        it 'returns middle position (4)' do
          expect(player.play).to eq 4
        end
      end

      context 'when middle position is filled' do
        let(:board) { build :board, :with_middle_filled }

        context 'when player can winner with a move' do
          before do
            board.play(player.marker, 0)
            board.play(player.marker, 1)
          end
          it 'returns the victory position' do
            expect(player.play).to eq 2
          end
        end

        context 'when opponent can win' do
          before do
            board.play('O', 0)
            board.play('O', 1)
          end

          it 'returns the victory position' do
            expect(player.play).to eq 2
          end
        end

        context "when does't a good move" do
          it 'returns a random valid position' do
            expect(board.available_positions).to include player.play
          end
        end
      end
    end
  end
end
