require 'spec_helper'

RSpec.describe Players::Human do

  describe 'inheritance' do
    it { expect(described_class).to be < Player }
  end

  describe 'methods' do
    subject(:player) { build :player_human }

    context '#play' do
      # Redirecionando stdin para evitar necessidade
      # de entrar com um valor no teste.
      before { $stdin = StringIO.new("4\n") }
      after { $stdin = STDIN }

      it 'returns an input' do
        expect(player.play).to eq 4
      end
    end
  end
end
