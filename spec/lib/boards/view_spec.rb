require 'spec_helper'

RSpec.describe Boards::View do
  describe 'instance methods' do
    subject(:board) { build :board }
    subject(:board_view) { build :board_view, board: board }

    describe '#simulate' do
      before do
        board.play('X', 0)
        board.play('X', 1)
      end

      it 'returns :win when simulate a victory position' do
        expect(board_view.simulate('X', 2)).to eq :win
        expect(board_view.available_position?(2)).to be true
        expect(board.available_position?(2)).to be true
      end

      it 'returns :open when simulate a normal position' do
        expect(board_view.simulate('X', 5)).to eq :open
        expect(board_view.available_position?(5)).to be true
        expect(board.available_position?(5)).to be true
      end
    end
  end
end
