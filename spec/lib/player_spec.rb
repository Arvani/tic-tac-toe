require 'spec_helper'

RSpec.describe Player do

  describe 'class methods' do
    describe '.oponent_marker' do
      context 'when passing X' do
        subject(:oponent_marker) { Player.oponent_marker('X') }
        it { is_expected.to eq 'O' }
      end

      context 'when passing O' do
        subject(:oponent_marker) { Player.oponent_marker('O') }
        it { is_expected.to eq 'X' }
      end
    end

    describe '.markers' do
      subject(:markers) { Player.markers }
      it { is_expected.to include 'X', 'O' }
    end

  end

  describe 'methods' do
    subject(:player) { build :player }

    context 'play' do
      it { expect{player.play}.to raise_error RuntimeError }
    end
  end
end
