require 'spec_helper'

RSpec.describe Board do
  describe 'instance methods' do
    subject(:board) { Board.new }

    describe '#finished' do
      let(:board) { build :board, :missing_x_in_position_2_for_victory }
      subject(:finished) { board.finished? }

      it { is_expected.to be false }

      context 'when someone wins' do
        before { board.play('X', 2) }

        it { is_expected.to be true }
      end

      context 'when tie' do
        let(:board) { build :board, :missing_8_to_tie }
        before { board.play('X', 8) }

        it { is_expected.to be true }
      end
    end

    describe '#available_position?' do
      before do
        board.play('X', 0)
        board.play('O', 2)
        board.play('X', 5)
      end

      it 'returns true when position is empty' do
        expect(board.available_position?(3)).to eq true
      end

      it 'returns false when position is already filled' do
        expect(board.available_position?(2)).to eq false
      end
    end

    describe '#available_positions' do
      before do
        board.play('X', 0)
        board.play('O', 2)
        board.play('X', 5)
      end

      it 'returns all available positions' do
        expect(board.available_positions).to eq [1, 3, 4, 6, 7, 8]
      end
    end

    describe '#play' do
      context 'when makes a valid play' do
        it 'returns true' do
          result = board.play('X', 4)

          expect(result).to be true
          expect(board.available_positions).not_to include 4
        end
      end

      context 'when makes an invalid play' do
        # Inserindo valor na posição 4,
        # garantindo que esteja ocupada
        before { board.play('X', 4) }

        it 'returns false' do
          result = board.play('X', 4)
          expect(result).to be false
        end
      end
    end

    describe '#show' do
      subject(:show) { board.show }

      context 'when is empty' do
        it 'returns an empty board' do
          is_expected.to eq <<~HEREDOC
            \n\n\t\b TIC TAC TOE \n
            \t\ 0 | 1 | 2
            \t===+===+===
            \t\ 3 | 4 | 5
            \t===+===+===
            \t\ 6 | 7 | 8
            \n
          HEREDOC
        end
      end

      context 'when there is some plays' do
        before do
          board.play('X', 4)
          board.play('0', 8)
        end

        it 'returns an board with markers' do
          is_expected.to eq <<~HEREDOC
            \n\n\t\b TIC TAC TOE \n
            \t\ 0 | 1 | 2
            \t===+===+===
            \t\ 3 | X | 5
            \t===+===+===
            \t\ 6 | 7 | 0
            \n
          HEREDOC
        end
      end
    end
  end
end
