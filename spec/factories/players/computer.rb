FactoryBot.define do
  factory :player_computer, parent: :player, class: Players::Computer do
    initialize_with { Players::Computer.new(marker, board_view) }
  end
end
