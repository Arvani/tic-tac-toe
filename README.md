# TIC TAC TOE
Jogo da velha utilizando orientação a objetos.

### Para jogar
`ruby app.rb`

### console
`irb -r console.rb`

## TODO
  - [ ] Criar classe `NotImplementedError` para métodos abstratos
  - [ ] Tornar classes `Computer` e `Player` abstratas
  - [ ] Criar método `expect` para classe `Array`
  - [ ] Criar classe especifica para `Marker`
  - [ ] Usar `delegator` para métodos de `Boards::View`
  - [ ] Finalizar testes para `Game` e `Config`
