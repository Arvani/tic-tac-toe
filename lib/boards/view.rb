module Boards
  #
  # Esta classe tem como objetivo passar informações do tabuleiro para Players,
  # protejendo a classe Board de qualquer mudaça ou aleração
  # (sendo esta função apenas da classe Game)
  #
  class View
    def initialize(board)
      @board = board
    end

    # TODO, Devemos usar delegator ao invés de usar este mé
    def available_positions
      @board.available_positions
    end

    # TODO, Devemos usar delegator ao invés de usar este mé
    def available_position?(position)
      available_positions.include? position
    end

    #
    # Simula uma jogada, analizando o situação que o tabuleiro terá.
    #
    # @param [String] marker Marcador a ser testado
    # @param [Integer] position Posição a ser testada
    #
    # @return [Symbol] Retorna situação do tabuleiro com a jogada testada
    #
    def simulate(marker, position)
      # Fazendo uma cópia para evitando a mudança de @board
      board = Board.new @board.positions.clone
      board.play marker, position
      board.state
    end
  end
end
