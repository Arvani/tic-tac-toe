#
# Classe responsável pelo controle do tabuleiro. Ela deve validar e registrar
# as jogadas pedidas. Também deve saber a situação do jogo
#
class Board
  attr_reader :state
  attr_reader :positions

  def initialize(positions = nil)
    @positions = positions ? positions : Array.new(9)
    @state = :open
  end

  def show
    <<~HEREDOC
      \n\n\t\b TIC TAC TOE \n
      \t\ #{show_position 0} | #{show_position 1} | #{show_position 2}
      \t===+===+===
      \t\ #{show_position 3} | #{show_position 4} | #{show_position 5}
      \t===+===+===
      \t\ #{show_position 6} | #{show_position 7} | #{show_position 8}
      \n
    HEREDOC
  end

  #
  # Realiza uma jogada válida.
  #
  # @param [String] marker Marcador usado para jogada
  # @param [Integer] position Posição do tabuleiro a ser preenchida
  #
  # @return [Boolean] Retorna booleano referente ao sucesso da jogada
  #
  def play(marker, position)
    return false unless valid_play? position
    @positions[position] = marker
    update_state

    true
  end

  #
  # @return [Array] Retorna array com todas as posições vazias do tabuleiro
  #
  def available_positions
    # Retornando todos os indexes que são nils. Ou seja
    # todas as posições que ainda estão vazias
    @positions.each_index.select{ |position| @positions[position] == nil }
  end

  def available_position?(position)
    available_positions.include? position
  end

  def finished?
    @state != :open
  end

  private

  #
  # Remove/volta posição no tabuleiro
  #
  # @param [Integer] position Posição a ser desconsiderada
  #
  def rollback(position)
    @positions[position] = nil
    update_state
  end

  #
  # Atualiza situação do tabuleiro de acordo com o tabuleiro
  #
  # @return [Symbol] Retorna situação da partida
  #
  def update_state
    directions.each do |direction|
      next unless same_values? direction
      return @state = :win
    end

    return @state = :tie if available_positions.empty?
    @state = :open
  end

  #
  # Direções possíveis para ganhar a partida
  #
  def directions
    [
      # Lines
      [@positions[0], @positions[1], @positions[2]],
      [@positions[3], @positions[4], @positions[5]],
      [@positions[6], @positions[7], @positions[8]],

      # Columns
      [@positions[0], @positions[3], @positions[6]],
      [@positions[1], @positions[4], @positions[7]],
      [@positions[2], @positions[5], @positions[8]],

      # Diagonals
      [@positions[0], @positions[4], @positions[8]],
      [@positions[2], @positions[4], @positions[6]]
    ]
  end


  #
  # Analiza se alguma direção tem todos os valores iguais e preenchidos
  #
  # @param [Array] arr Array/Direção a ser analisada
  #
  # @return [Boolan] Retorna booleano com resultado
  #
  def same_values?(arr)
    return false if arr.include? nil
    arr.uniq.length == 1
  end

  def show_position(position)
    @positions[position] || position
  end

  #
  # Analisa se jogada é valida
  #
  # @param [Integer] position Posição a ser avaliada
  #
  # @return [Boolean] Retorna booleano com resultado
  #
  def valid_play?(position)
    @positions[position].nil?
  end
end
