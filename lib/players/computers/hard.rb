require_relative '../computer.rb'

module Players
  module Computers
    #
    # Nivel Dificil de dificuldade
    #
    class Hard < Computer
      def initialize(marker, board_view)
        super

        # Definindo 100% de possibilidade de realizar a melhor jogada
        @difficulty = 1
      end
    end
  end
end
