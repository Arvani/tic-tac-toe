require_relative '../computer.rb'

module Players
  module Computers
    #
    # Nivel Fácil de dificuldade
    #
    class Easy < Computer
      def initialize(marker, board_view)
        super

        # Definindo 10% de possibilidade de realizar a melhor jogada
        @difficulty = 0.1
      end
    end
  end
end
