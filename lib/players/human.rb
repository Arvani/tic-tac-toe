require_relative '../player'

#
# Classe responsável por controlar um Jogador Humano na partida.
#
module Players
  class Human < Player

    def play
      position = false
      position = gets.chomp.to_i until @board_view.available_position? position
      position
    end
  end
end
