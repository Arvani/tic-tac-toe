#
# Classe base para Player. Portanto todo player (computer e human)
# deve extender dela.
#
class Player
  attr_reader :marker

  # TODO, Criar classe Marker
  MARKERS = %w[X O].freeze

  def initialize(marker, board_view)
    @marker = marker
    @board_view = board_view
  end

  def play
    # TODO, Criar classe NotImplementedError
    raise "#{self.class.name}#play is an abstract method."
  end

  # TODO, Criar ext/array.rb adicionando método except
  # atualizando este código com markers.except(marker).first
  def self.oponent_marker(marker)
    (markers - [marker]).first
  end

  def self.markers
    MARKERS
  end
end
